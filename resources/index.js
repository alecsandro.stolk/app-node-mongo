module.exports = function (app) {
    app.use('/api/v1/emprestimos', require('../models/emprestimos'));
    app.use('/api/v1/pessoas', require('../models/pessoas'));
};
