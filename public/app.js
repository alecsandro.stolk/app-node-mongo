(function(){
    angular.module('minhaApp', [
        'ui.router'
    ]);

    angular.module('minhaApp')
        .config(MinhaAppConfig);
    
    MinhaAppConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
    
    function MinhaAppConfig($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state({
                name: 'login', 
                url: '/login',
                templateUrl: '/views/login.html',
                controller: 'LoginController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app', 
                url: '/',
                templateUrl: '/views/menu.html'
            })
            .state({
                name: 'app.clientes', 
                url: 'clientes',
                template: '<ui-view></ui-view>',
                redirectTo: 'app.clientes.list'
            })
            .state({
                name: 'app.clientes.list', 
                url: '/list',
                templateUrl: '/views/clientes/list.html',
                controller: 'ClientesListController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app.clientes.new', 
                url: '/new',
                templateUrl: '/views/clientes/form.html',
                controller: 'ClientesFormController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app.clientes.edit', 
                url: '/:id',
                templateUrl: '/views/clientes/form.html',
                controller: 'ClientesFormController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app.produtos', 
                url: 'produtos',
                template: '<ui-view></ui-view>',
                redirectTo: 'app.produtos.list'
            })
            .state({
                name: 'app.produtos.list', 
                url: '/list',
                templateUrl: '/views/produtos/list.html',
                controller: 'ProdutosListController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app.produtos.new', 
                url: '/new',
                templateUrl: '/views/produtos/form.html',
                controller: 'ProdutosFormController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app.produtos.edit', 
                url: '/:id',
                templateUrl: '/views/produtos/form.html',
                controller: 'ProdutosFormController',
                controllerAs: 'vm'
            })
    
    }
})();
