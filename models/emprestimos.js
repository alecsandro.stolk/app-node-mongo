const mongose = require('mongose');

const tipos = [
    "CONCEDIDO",
    "RECEBIDO"
];

const emprestimosSchema = new mongoose.Schema({
    tipo: {
        type: String,
        required: true,
        enum: tipos 
    },
    pessoa: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Pessoas'
    },
    objeto: {
        type: String,
        required: true
    },
    dataEmprestimo: {
        type: date,
        required: true
    },
    dataDevolucao: {
        type: date,
        required: true
    },
    observacao: {
        type: String
    }

},
{ versionKey: false,
  autoIndex: true,
  background: true  }
 )

const emprestimos = mongose.model('Emprestimos', emprestimosSchema)
module.exports = emprestimos;