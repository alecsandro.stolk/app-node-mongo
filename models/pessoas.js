var mongoose = require('mongoose');

var pessoasSchema = new mongoose.Schema({
    nome: {
      type: String,
      required: true,
      trim: true,
      index: true
    },
    email: {
        type: String,
        required: false
    },
    telefone: {
        type: String,
        required: true
    }
},
{ versionKey: false },
{ autoIndex: false });

module.exports = mongoose.model('Pessoas', pessoasSchema);
