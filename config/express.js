var express    = require('express');
var http       = require('http');
var HTTPStatus = require('http-status');
var mongoose   = require('mongoose');
var bodyParser = require('body-parser');
var morgan     = require('morgan');
var consign    = require('consign');

var app        = express();
var server     = http.createServer(app);

// middlewares
app.use(morgan('dev'));
app.use(express.static('./public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

consign()
  .include('models')
  .then('resources')
  .into(app);

// database
var mongoDb = mongoose.connect('mongodb://localhost/emprestimos').connection;

mongoDb.on('connected', function() {
   console.log('Conectado com sucesso no banco');
});

mongoDb.on('error', function() {
   console.log('MongoDB ERROR');
});

// add routes
require('../resources/index')(app);

// fire up express
server.listen(3000, function() {
   console.log('Servidor de aplicação iniciado com sucesso!');
});
